﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_decrementation = New System.Windows.Forms.Button()
        Me.btn_incrementation = New System.Windows.Forms.Button()
        Me.lb_total = New System.Windows.Forms.Label()
        Me.lb_compte = New System.Windows.Forms.Label()
        Me.btn_raz = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn_decrementation
        '
        Me.btn_decrementation.Location = New System.Drawing.Point(54, 136)
        Me.btn_decrementation.Name = "btn_decrementation"
        Me.btn_decrementation.Size = New System.Drawing.Size(75, 23)
        Me.btn_decrementation.TabIndex = 0
        Me.btn_decrementation.Text = "-"
        Me.btn_decrementation.UseVisualStyleBackColor = True
        '
        'btn_incrementation
        '
        Me.btn_incrementation.Location = New System.Drawing.Point(290, 136)
        Me.btn_incrementation.Name = "btn_incrementation"
        Me.btn_incrementation.Size = New System.Drawing.Size(75, 23)
        Me.btn_incrementation.TabIndex = 1
        Me.btn_incrementation.Text = "+"
        Me.btn_incrementation.UseVisualStyleBackColor = True
        '
        'lb_total
        '
        Me.lb_total.AutoSize = True
        Me.lb_total.Location = New System.Drawing.Point(191, 65)
        Me.lb_total.Name = "lb_total"
        Me.lb_total.Size = New System.Drawing.Size(31, 13)
        Me.lb_total.TabIndex = 2
        Me.lb_total.Text = "Total"
        '
        'lb_compte
        '
        Me.lb_compte.AutoSize = True
        Me.lb_compte.Location = New System.Drawing.Point(200, 141)
        Me.lb_compte.Name = "lb_compte"
        Me.lb_compte.Size = New System.Drawing.Size(13, 13)
        Me.lb_compte.TabIndex = 3
        Me.lb_compte.Text = "0"
        '
        'btn_raz
        '
        Me.btn_raz.Location = New System.Drawing.Point(167, 200)
        Me.btn_raz.Name = "btn_raz"
        Me.btn_raz.Size = New System.Drawing.Size(75, 23)
        Me.btn_raz.TabIndex = 4
        Me.btn_raz.Text = "RAZ"
        Me.btn_raz.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(422, 292)
        Me.Controls.Add(Me.btn_raz)
        Me.Controls.Add(Me.lb_compte)
        Me.Controls.Add(Me.lb_total)
        Me.Controls.Add(Me.btn_incrementation)
        Me.Controls.Add(Me.btn_decrementation)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_decrementation As Button
    Friend WithEvents btn_incrementation As Button
    Friend WithEvents lb_total As Label
    Friend WithEvents lb_compte As Label
    Friend WithEvents btn_raz As Button
End Class
