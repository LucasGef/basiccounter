﻿Imports Compteur

Public Class Form1
    Private Sub btn_incrementation_Click(sender As Object, e As EventArgs) Handles btn_incrementation.Click
        lb_compte.Text = Comptage.increment()
    End Sub

    Private Sub btn_decrementation_Click(sender As Object, e As EventArgs) Handles btn_decrementation.Click
        lb_compte.Text = Comptage.decrement()
    End Sub

    Private Sub btn_raz_Click(sender As Object, e As EventArgs) Handles btn_raz.Click
        lb_compte.Text = Comptage.raz()
    End Sub
End Class
