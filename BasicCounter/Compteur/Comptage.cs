﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compteur
{
    public class Comptage
    {
        public static int Valeur = 0;
        public static int increment()
        {
            Valeur = Valeur + 1;
            return Valeur;
        }
        public static int decrement()
        {
            if (Valeur > 0)
            {
            Valeur = Valeur - 1;
            }
            return Valeur;
        }
        public static int raz()
        {
            return 0;
        }
    }
}
