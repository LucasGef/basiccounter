﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Compteur;

namespace CompteurTests
{
    [TestClass]
    public class OperationsTest
    {
        [TestMethod]
        public void Testdecrementation()
        {
            Assert.AreEqual(1, Comptage.increment());
            Assert.AreEqual(0, Comptage.decrement());

        }
        [TestMethod]
        public void Testincrementation()
        {
            Assert.AreEqual(1, Comptage.increment());
            Assert.AreEqual(2, Comptage.increment());
        }
        [TestMethod]
        public void Testraz()
        {
            Assert.AreEqual(0, Comptage.raz());
        }
    }
}
